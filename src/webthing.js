import {
  Property,
  Thing,
  Value,
} from 'webthing';

function getProperties() {
  const now = new Date();
  return {
    dayOfWeek: now.getDay(),
    isWeekday: [1,2,3,4,5].includes(now.getDay()),
  }
}

export default class WebThingDayOfWeek extends Thing {
  constructor(options = {},) {
    super(
      options.id || 'urn:dev:ops:day-of-week',
      options.title || 'Day of week',
      ['Thing'],
      options.description || 'Determines the day of the week and whether it is a weekday'
    );

    const properties = getProperties();
    this.dayOfWeek = new Value(properties.dayOfWeek);
    this.isWeekday = new Value(properties.isWeekday);

    this.addProperty(
      new Property(this,
        'day-of-week',
        this.dayOfWeek,
        {
          title: 'days since Sunday',
          type: 'integer',
          description: 'Counts the days since Sunday (0 until 6)',
          minimum: 0,
          maximum: 6,
          readOnly: true,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'is-weekday',
        this.isWeekday,
        {
          title: 'is a weekday',
          type: 'boolean',
          description: 'Whether it is a weekday (Monday until Friday)',
          readOnly: true,
        }
      )
    );

    setInterval(() => {
      this.updateProperties();
    }, 1000);
  }

  updateProperties() {
    const properties = getProperties();
    this.dayOfWeek.notifyOfExternalUpdate(properties.dayOfWeek);
    this.isWeekday.notifyOfExternalUpdate(properties.isWeekday);
  }
}
