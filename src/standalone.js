import {
  SingleThing,
  WebThingServer,
} from 'webthing';
import WebThing from './webthing.js';

function runServer(port) {
  const thing = new WebThing();

  const server = new WebThingServer(new SingleThing(thing), port);

  process.on('SIGINT', () => {
    server.stop();
    process.exit();
  });

  server.start();
}

runServer(process.env.PORT || 8000);
