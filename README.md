# webthing-day-of-week
This is a read-only Web Thing to determine the day of the week and whether it is a weekday. Use it as an input for rules in your Web of Things gateway.

![A preview image of the Web Thing Day of the Week in the Mozilla Things Gateway](preview.png)

## Explanation
The property `days since Sunday` counts the days since Sunday. Its values range from 0 on Sundays to 6 on Saturdays. This can be used to test the day of the week.

For simpler checking of the day of the week there is the property `is weekday`. It’s a true/false value and indicates whether the current day is a weekday (Monday until Friday, inclusive).

## Installation
Download the repository into a folder and install the dependencies. You may do this by running:

```
# Download the repository via git
git clone https://gitlab.com/webthings/webthing-day-of-week.git

# Install project dependencies
npm ci
```

## Run
```
# Specify a port in Bash (Linux and Mac)
#export PORT=8000
# Specify a port in Batch (Windows)
#SET /A PORT=8000

npm start
```

This starts a web server on your machine. By default it starts on TCP port 8000. Determine your system’s local IP address and add the Web Thing to your gateway.

### Trouble shooting
#### It fails to start with `Error: listen EADDRINUSE :::8000`
You already have an instance of this project or another application running on the mentioned TCP port. On a computer a port cannot be assigned to two servers.  
On Linux you can identify open TCP ports and the owner process with the command `netstat -lnpt`.

#### The server is not reachable from another computer
Ensure that the computers are on the same network. Test their connectivity with the `ping` command. If they are connected, the computer running the server might block the TCP port with its firewall.
