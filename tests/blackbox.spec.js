import {
  SingleThing,
  WebThingServer,
} from 'webthing';
import WebThingHTTPTester from 'webthing-http-tester';

import WebThing from '../src/webthing.js';


const PORT = 8000;

describe('test', () => {
  const thing = new WebThing();
  let server = new WebThingServer(new SingleThing(thing), PORT);
  let tester = new WebThingHTTPTester({port: PORT});

  beforeAll(async () => {
    await server.start();
  });
  afterAll(async () => {
    await server.stop();
  });
  test('matches snapshot', async () => {
    const data = await tester.getResponse('/');
    data.links = '##redacted##';
    expect(data).toMatchSnapshot();
  });
  test('has no actions', async () => {
    const actions = await tester.getResponse('/actions');
    expect(actions).toHaveLength(0);
  });
  test('has no events', async () => {
    const events = await tester.getResponse('/events');
    expect(events).toHaveLength(0);
  });
  test('has the defined properties', async () => {
    const properties = await tester.getResponse('/properties');
    expect(properties).toHaveProperty('day-of-week');
    expect(properties).toHaveProperty('is-weekday');
  });
});
