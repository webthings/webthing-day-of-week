## Next release

- Things now require an `id`. I've set the default to `urn:dev:ops:day-of-week`.
- Things now use `title` rather than `name` for the thing and all properties.
- Now a JavaScript module.

## 0.1.3 (2019-01-20)

- Update dependency webthing to 0.11.0

## 0.1.2 (2018-12-30)

- Update dependency webthing to 0.10.0
- Add testing

## 0.1.1 (2018-09-22)

- Update dependency webthing to 0.8.0
- Implement readOnly flag
